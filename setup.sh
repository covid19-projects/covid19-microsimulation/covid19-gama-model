#!/bin/bash

sudo apt update &&
sudo apt upgrade -y &&

wget -O gama_platform.zip https://github.com/gama-platform/gama/releases/download/latest/GAMA1.8_Official_withJDK_Linux_64bits.zip &&
unzip -d gama_platform gama_platform.zip &&
rm gama_platform.zip &&
rm gama_platform/headless/gama-headless.sh &&

cp setup_files/gama-headless.sh gama_platform/headless/gama-headless.sh &&
chmod 774 gama_platform/headless/gama-headless.sh &&
chmod 774 gama_platform/jdk/bin/java