/***
* Name: DemographicRange
* Author: Ezequiel Puerta
* Description: Rango Demografico
***/

model DemographicRange

import "../../core/Constants.gaml"

species demographic_range {	
	int min_age;
	int max_age;
	string consider_as;
	float total_percentage;
	float man_percentage;
	float woman_percentage;
	float man_fatality_rate;
	float woman_fatality_rate;
	float st_dev_for_man_fatality_rate;
	float st_dev_for_woman_fatality_rate;
	
	bool is_young {return self.consider_as = young;}
	bool is_adult {return self.consider_as = adult;}
	bool is_older_adult {return self.consider_as = older_adult;}
	
	int generate_age {return rnd(self.min_age, self.max_age);}
	string generate_sex {if flip(self.man_percentage) {return male;} else {return female;}}
}