/***
* Name: Road
* Author: Ezequiel Puerta
* Description: Camino generico
***/

model Road

species road {
    aspect geom {
    	draw shape color: #black at: location + {0,0,9};
    }
}