/***
* Name: CensusRadio
* Author: Ezequiel Puerta
* Description: Radios censales (conjunto de manzanas) con igual Indice de Calidad de Vida (Censo 2010)
***/

model CensusRadio

import "../targets/House.gaml"
import "../../core/Functions.gaml"

species census_radio {
	float icv_value;
	float density;
	int population;
	int houses_amount;
	list<house> houses;
	
	float value {return icv_value;}
	
    aspect geom_icv {
    	list<rgb> colors <- [#maroon, #darkred, #firebrick, #red, #orangered, #orange, #yellow, #greenyellow, #lime, #limegreen];
    	rgb current_color <- world.current_color_of(icv_value, 10.0, colors);
    	draw shape color: current_color at: location + {0,0,1};
    }
    
    aspect geom_density {
    	list<rgb> colors <- [#white, #lightyellow, #lemonchiffon, #khaki, #yellow, #gold, #orange, #orangered, #red, #darkred];
    	rgb current_color <- world.current_color_of(density, max_density, colors);
    	draw shape color: current_color at: location + {0,0,1};
    }
}