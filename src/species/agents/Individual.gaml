/***
* Name: Individual
* Author: Ezequiel Puerta
* Description: Individuals
***/

model Individual

import "../adhoc/BiologicalOrganism.gaml"
import "../targets/AbstractTarget.gaml"
import "../../policies/activities/Activity.gaml"
import "../../core/Functions.gaml"

species individual schedules: (individual where ! each.bio.is_dead()) skills:[moving] {
	/* General Attributes */
	house home;
	float speed <- individual_speed;
	list<activity> activities;
	abstract_target current_objective;
	scheduled_task doing;
	string contagion_type;
	list<individual> infections;
	
	/* Begin of Dispatch to Biological Organism */
	biological_organism bio;
	action set_demographic_range(demographic_range range) {
		do load_demographic_data(range, range.generate_age(), range.generate_sex());}
		
	action load_demographic_data(demographic_range my_range, int my_age, string my_sex) {
		self.bio.range <- my_range;
		self.bio.age <- my_age;
		self.bio.sex <- my_sex;}
	
	init {create biological_organism {myself.bio <- self; self.person <- myself;}}
	
	bool is_young {return self.bio.is_young();}
	bool is_adult {return self.bio.is_adult();}
	bool is_older_adult {return self.bio.is_older_adult();}
	/* End of Dispatch to Biological Organism */
	
	/* Testing */
	bool is_doing_something {return self.doing != nil;}
	bool is_unemployed {return (self.is_adult() or self.is_older_adult()) and (!(self.bio.is_dead()) and (self.activities none_matches each.for_work()));}
	
	bool is_resting {return !self.bio.is_dead() and !self.is_doing_something();}
	bool is_moving {return !self.bio.is_dead() and self.doing.is_moving();}
	bool is_studying {return !self.bio.is_dead() and self.is_doing_something() and self.doing.task.for_study() and self.doing.is_executing();}
	bool is_working {return !self.bio.is_dead() and self.is_doing_something() and self.doing.task.for_work() and self.doing.is_executing();}
	bool is_out_of_map {return !self.bio.is_dead() and self.is_doing_something() and self.doing.task.for_out_of_map() and self.doing.is_executing();}
	bool is_supplying {return !self.bio.is_dead() and self.is_doing_something() and self.doing.task.for_supplying() and self.doing.is_executing();}
	bool is_playing_at_park {return !self.bio.is_dead() and self.is_doing_something() and self.doing.task.for_playing_at_park() and self.doing.is_executing();}
	bool is_in_quarantine {return !self.bio.is_dead() and self.is_doing_something() and self.doing.task.for_quarantine() and self.doing.is_executing();}
	bool is_in_treatment {return !self.bio.is_dead() and self.is_doing_something() and self.doing.task.for_treatment() and self.doing.is_executing();}
	
	bool is_local_case {return self.contagion_type = local_case;}
	bool is_imported_case {return self.contagion_type = imported_case;}
	bool is_external_contact_case {return self.contagion_type = external_contact_case;}
	bool is_contamination_case {return self.contagion_type = contamination_case;}
	
	/* Accessing */
	float icv_value {return self.home.zone.icv_value;}
	
	/* Disease Progress */
	bool will_get_infected {
		return flip(world.affect_rate_by_icv(self, transmission_ratio));}
	
	action get_infected_as (string case_type) {
		self.bio.infection_date <- current_date;
		self.contagion_type <- case_type;}
	
	action infect {
		if self.bio.is_infectious() and !self.is_out_of_map() {
			ask world.individuals_around_person(self, contact_distance) {
				if self.bio.is_susceptible() and self.will_get_infected() {
					do get_infected_as(local_case);
					world.disease_cases_evolution << [self.name, local_case, myself.name, world.as_wkt_point(self.location)];
					add item: self to: myself.infections;}}}}
	
	/* Movements */	
	reflex move when: (self.doing.is_moving() and not(self.doing.restricted)) {do goto target: self.doing.target on: street_network;}
	
	/* Aspect */
	aspect geom {
		rgb current_color <- #black;
		int z_value <- 0;
		switch self.bio.state {
			match "susceptible" {current_color <- susceptible_color; z_value <- 10;}
			match "exposed" {current_color <- exposed_color; z_value <- 12;}
			match_one ["asymptomatic", "presymptomatic", "mild_symptomatic", "severe_symptomatic", "icu_symptomatic"] {current_color <- infectious_color; z_value <- 13;}
			match "recovered" {current_color <- recovered_color; z_value <- 11;}
			match "dead" {current_color <- dead_color; z_value <- 14;}
		}
		
		if self.is_young() {draw square(display_size * 2 #m) color: current_color border: #black at: location + {0,0,z_value};} 
		else if self.is_adult() {draw circle(display_size #m) color: current_color border: #black at: location + {0,0,z_value};}
		else if self.is_older_adult() {draw triangle(display_size * 2 #m) color: current_color border: #black at: location + {0,0,z_value};}
	}
}