/***
* Name: House
* Author: Ezequiel Puerta
* Description: Hogares
***/

model House

import "../agents/Individual.gaml"

species house parent: concrete_target {
	list<individual> inhabitants;
	geometry shape;
	census_radio zone;
	
	/* Visiting */
	action arrives(individual person) {invoke arrives(person);}
	
	aspect geom {
		draw shape color: #silver at: location + {0,0,5};
	}

}