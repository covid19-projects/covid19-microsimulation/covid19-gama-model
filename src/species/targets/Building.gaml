/***
* Name: Building
* Author: Ezequiel Puerta
* Description: Edificios residenciales, de trabajo o proposito general
***/

model Building

import "ConcreteTarget.gaml"

species building parent: concrete_target {
	string name_id;
	int maximum_staff_amount;
	
	/* Visiting */
	action arrives(individual person) {invoke arrives(person);}
	
	aspect geom {
		draw shape color: #dimgray at: location + {0,0,4};
	}

}