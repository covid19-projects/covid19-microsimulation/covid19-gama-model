/***
* Name: AbstractTarget
* Author: Ezequiel Puerta
* Description: Abstract Target
***/

model AbstractTarget

import "../agents/Individual.gaml"

species abstract_target {
	geometry shape;
	string type_activity;
	bool is_for_essential_activity <- false;
	
	/* Testing */
	bool is_concrete_target {return false;}
	
	/* Visiting */
	action arrives(individual person) virtual: true;
	action leaves(individual person) virtual: true;
	
	/* Aspect */
	aspect default {
		draw shape color: #dimgray empty: true at: location + {0,0,4};
	}
}