/***
* Name: Vehicle
* Author: Ezequiel Puerta
* Description: Vehiculo
***/

model Vehicle

import "../../core/Constants.gaml"

species vehicle skills:[moving] {
	bool driver_exposed;
	point origin;
	point target;
	
	reflex move {
		do goto target: target on: routes_network;
		if (location = target) {do die;}}
	
	reflex infect when: self.driver_exposed {
		ask world.individuals_around_vehicle(self, contact_distance*10) {
			if self.will_get_infected() {
				do get_infected_as(external_contact_case);
				world.disease_cases_evolution << [self.name, external_contact_case, myself.name, world.as_wkt_point(self.location)];}}}
	
	aspect geom {
		if dead(self) or !self.driver_exposed {
			draw square(display_size * 3 #m) color: #white border: #black at: location + {0,0,50};
		} else {
			draw square(display_size * 3 #m) color: infectious_color border: #black at: location + {0,0,50};
		}
	}
}