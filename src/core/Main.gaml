/***
* Name: Main
* Author: Ezequiel Puerta
* Description: Simulación principal
***/

model Main

import "../experiments/BuildingsViralLoad.gaml"
import "../experiments/GIS_Movements.gaml"
import "../experiments/HospitalCapacity.gaml"
import "../experiments/MapUI.gaml"
import "../experiments/MonitorsExperiment.gaml"
import "../experiments/ParameterExploration.gaml"
import "../experiments/ParameterOptimization.gaml"

global {}