/***
* Name: Constants
* Author: Ezequiel Puerta
* Description: Constants
***/

model Constants

import "../species/agents/Individual.gaml"
import "../policies/Scheduler.gaml"
import "../core/ExportCSV.gaml"

global {
	/* Age range */
	string young <- "young";
	string adult <- "adult";
	string older_adult <- "older_adult";
	
	/* Sex */
	string male <- "M";
	string female <- "F";
	
	/* Disease status */
	string susceptible <- "susceptible";
	string exposed <- "exposed";
	string asymptomatic <- "asymptomatic";
	string presymptomatic <- "presymptomatic";
	string mild_symptomatic <- "mild_symptomatic";
	string severe_symptomatic <- "severe_symptomatic";
	string icu_symptomatic <- "icu_symptomatic";
	string recovered <- "recovered";
	string dead <- "dead";
	
	/* Contagion types */
	string initial_case <- "Initial case";
	string local_case <- "Local case";
	string imported_case <- "Imported case";
	string external_contact_case <- "External contact case";
	string contamination_case <- "Building contamination case";
	
	/* Treatment types */
	int no_treatment <- 0;
	int severe_treatment <- 1;
	int icu_treatment <- 2;
	
	/* Activities status */
	string studying <- "studying";
	string working <- "working";
	string quarantine <- "quarantine";
	string treatment <- "treatment";	
	string out_of_map <- "out_of_map";
	string supplying <- "supplying";
	string playing_at_park <- "playing_at_park";
	
	/* Scheduled Tasks */
	string idle <- "idle";
	string pre_executing <- "pre_executing";
	string executing <- "executing";
	string post_executing <- "post_executing";
	
	/* Generic buildings activity type */
	string security_b <- "security";
	string firefighters_b <- "firefighters";
	string government_b <- "government";
	string factory_b <- "factory";
	string banking_b <- "banking";
	string supplying_b <- "supplying";

	/* Specific buildings activity type */	
	string kindergarden_b <- "kindergarden";
	string school_b <- "school";
	string high_school_b <- "high_school";
	string adult_high_school_b <- "adult_high_school";
	string university_b <- "university";
	string station_b <- "station";
	string hospital_b <- "hospital";
	string house_b <- "house";
	string park_b <- "park";
	
	/* Colors */
	rgb susceptible_color <- #limegreen;
	rgb exposed_color <- #gold;
	rgb infectious_color <- #orangered;
	rgb recovered_color <- #deepskyblue;
	rgb dead_color <- #black;
	
	/* Execution mode */
	string local <- "Local";
	string cloud <- "Cloud";
	string parallel <- "Parallel";
	
	/* Time parameters */
	date starting_date <- date("2020-03-20 00:00:00");
	float step <- 60.0 #minutes;
	int steps_between_collect <- 0;
	int steps_between_dump <- 5;
	int steps_on_a_day <- int(1 #day / step);
	int steps_on_one_hour <- int(1 #hour / step);
	int steps_on_ten_minutes <- int(10 #minutes / step);
	float steps_on_a_minute <- 1 #minute / step;
	float days <- 0 #days;
	bool is_new_day {return mod(cycle, steps_on_a_day)=0 and cycle!=0;}
	reflex advance_days when: is_new_day() {days <- days + 1 #day;}
	int monday <- 0;
	int tuesday <- 1;
	int wednesday <- 2;
	int thursday <- 3;
	int friday <- 4;
	int saturday <- 5;
	int sunday <- 6;
	list<int> week <- [monday, tuesday, wednesday, thursday, friday];
	list<int> weekend <- [saturday, sunday];
	action is_weekend(int current_day) {return weekend one_matches (each = current_day);}

	/* Experiment parameters */
	int individual_amount <- 0;
	int display_size <- 30;
	float individual_speed <- 2.0 #km/#h;
	point autosave_size <- {900,900};
	
	string mode <- local;
	int final_step <- 1440;
	int repeat <- 3;
	int iter_max <- 50;
	int real_symptomatic_amount;
	int real_dead_amount;
	
	float min_transmission_ratio;
	float max_transmission_ratio;
	float step_transmission_ratio;
	float min_asymptomatic_ratio;
	float max_asymptomatic_ratio;
	float step_asymptomatic_ratio;
	float min_restrictions_compliance_ratio;
	float max_restrictions_compliance_ratio;
	float step_restrictions_compliance_ratio;
	float min_self_quarantine_compliance_ratio;
	float max_self_quarantine_compliance_ratio;
	float step_self_quarantine_compliance_ratio;
	
	reflex end_simulation when: ((individual count each.bio.is_infected()) = 0) and (individual_amount != (individual count each.bio.is_susceptible())) {
		loop sim over: ExportCSV_model {
			ask sim.house {
				ask world {
					do dump_csv;
					do pause;}}}}
	
	/* Epidemiological parameters */
	float contact_distance <- 2.0 #meters;
	int patient_zero_amount <- 2;
	float transmission_ratio <- 0.1;
	float asymptomatic_ratio <- 0.80;
	float mild_ratio <- 0.80;
	float severe_ratio <- 0.15;
	float icu_ratio <- 0.05;
	float mean_latent_days <- 5.0 #days;
	float st_deviation_latent_days <- 0.5 #days;
	float mean_incubation_days <- 7.0 #days;
	float st_deviation_incubation_days <- 1.0 #days;
	float mean_mild_recovery_days <- 11.1 #days;
	float st_deviation_mild_recovery_days <- 2.0 #days;
	float mean_severe_recovery_days <- 28.6 #days;
	float st_deviation_severe_recovery_days <- 3.0 #days;
	float mean_icu_recovery_days <- 29.1 #days;
	float st_deviation_icu_recovery_days <- 3.0 #days;
	float days_on_surfaces <- 3.0 #days;
	float expected_contaminated_surface <- 0.1;
	
	list<unknown> disease_states_evolution <- [];
	list<unknown> disease_cases_evolution <- [];
	list<unknown> parameter_optimization_evolution <- [];
	
	/* Activities parameters */
	scheduler scheduler_singleton;
	int external_workers_amount <- 0;
	int essential_workers_amount <- 0;
	float supplying_ratio <- 0.005;
	
	/* Restrictions parameters */
	float restrictions_compliance_ratio <- 0.6;
	bool allow_excepted <- true;
	bool people_get_self_quarantine <- true;
	float self_quarantine_compliance_ratio <- 0.95;
	bool schools_are_open <- true;
	float travel_radius <- 10.0 #km;
	
	/* Demographic parameters */
	map<pair<string, string>, list<individual>> synthetic_population;
	map<int, map<map<pair<string, string>, int>, float>> possible_families;
	list<demographic_range> demographic_data;
	int young_male_amount <- -1;
	int adult_male_amount <- -1;
	int older_adult_male_amount <- -1;
	int young_female_amount <- -1;
	int adult_female_amount <- -1;
	int older_adult_female_amount <- -1;
		
	/* Geospatial parameters */
	string coordinate_system_input <- "EPSG:3857";
	string coordinate_system_output <- "EPSG:4326";
	graph street_network;
	graph routes_network;
	float max_density <- 0.0;
	float max_icv <- 10.0;
	
	/* File Parameters */
	XMLFile xml_headless_file;
	string xml_headless_file_path <- '';
	string snapshot_file_path <- '';
			
	string extra_parameters_json_file_path <- nil;
	string output_path <- "../use_cases/";
	string satellital_image_file_path;
	string streets_shapefile_file_path;
	string routes_shapefile_file_path;
	string houses_shapefile_file_path;
	string blocks_shapefile_file_path;
	string workplaces_shapefile_file_path;
	string hospitals_shapefile_file_path;
	string train_stations_shapefile_file_path;
	string frontiers_shapefile_file_path;
	string census_radios_shapefile_file_path;
	
	image_file satellital_image;
	shape_file streets_shapefile;
	shape_file routes_shapefile;
	shape_file houses_shapefile;
	shape_file blocks_shapefile;
	shape_file workplaces_shapefile;
	shape_file hospitals_shapefile;
	shape_file train_stations_shapefile;
	shape_file frontiers_shapefile;
	shape_file census_radios_shapefile;
}