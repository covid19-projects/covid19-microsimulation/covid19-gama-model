/***
* Name: Functions
* Author: Ezequiel Puerta
* Description: Funciones
***/

model Functions

import "Constants.gaml"
import "../species/geography/CensusRadio.gaml"

global {
	/********************************** Agents *********************************/
	individual individual_named(string individual_name) {
		return individual at (int(replace(individual_name, string(individual), "")));}
		
	house house_named(string house_name) {
		return house at (int(replace(house_name, string(house), "")));}
	
	/*********************************** Time **********************************/
	float days_between(date first_date, date last_date) {
		return ((last_date - first_date) / 1 #day) #days;
	}
	
	float days_since(date past_date) {
		return days_between(past_date, world.current_date);
	}
	
	string elapsed_time {
		float elapsed <- world.current_date - world.starting_date;
		return 
			" | Days: " + string(int(floor(elapsed / 1 #day))) +
			" | Hours: " + string(int(elapsed / 1 #hour) mod 24) +
			" | Minutes: " + string(int(elapsed / 1 #minute) mod 60);
	}
	
	bool is_time_to_collect {
		if steps_between_collect = 0 {return true;} else {return mod(int(time), int(steps_between_collect * step)) = 0;}}
	bool is_time_to_dump {
		if steps_between_dump = 0 {return true;} else {return mod(int(time), int(steps_between_dump * step)) = 0;}}
	
	/****************************** Execution Mode *****************************/
	bool is_running_multiple_instances {return world.mode = world.parallel;}
			
	string output_path_for(string simulation_name, string folder, string csv_file_name) {
		string full_output_path;
		if is_running_multiple_instances() {full_output_path <- output_path + folder + csv_file_name;} 
		else {full_output_path <- output_path + folder + simulation_name + "/" + csv_file_name;}
		return full_output_path;}
		
	string output_csv_path_for(string simulation_name, string csv_file_name) {
		return output_path_for(simulation_name, "/csv/", csv_file_name);}
	
	/******************************* Probability *******************************/
	float normal_from(float mean, float st_deviation) {
		return abs(gauss(mean, st_deviation));}
	
	unknown multinomial_from(map<unknown, float> probabilities) {
		float random_number <- rnd(1.0);
		float lower_bound <- 0.0;
		float upper_bound <- 0.0;
		loop probability_pair over: probabilities.pairs {
			upper_bound <- upper_bound + float(probability_pair.value);
			if lower_bound <= random_number and random_number <= upper_bound {return probability_pair.key;} 
			else {lower_bound <- upper_bound;}}}
	
	/******************************** Population *******************************/
	int max_family_size <- 6;
	
	demographic_range demographic_range_according(int an_age) {
		return any(world.demographic_data where (each.min_age <= an_age and (an_age <= each.max_age)));}
	
	demographic_range get_demographic_range {
		assert length(world.demographic_data) > 0;
		map<demographic_range, float> probabilities;
		loop range over: world.demographic_data {add range::range.total_percentage to: probabilities;}
		return multinomial_from(probabilities);}
						
	int assign_families_inside(int family_size, house a_home) {
		int real_family_size <- 0;
			
		if family_size > 0 {			
			map<pair<string, string>, int> family_type <- multinomial_from(world.possible_families at family_size);
		
			loop individual_type over: family_type.pairs {
				int selected_amount <- length(world.synthetic_population at pair<string, string>(individual_type.key));
				if selected_amount >= int(individual_type.value) {
					loop i from: 0 to: int(individual_type.value)-1 {
						individual current_individual <- last(world.synthetic_population at individual_type.key);
						ask world {
							do assign_individual_inside(current_individual, a_home); 
							real_family_size <- real_family_size+1;}
						remove index: selected_amount-i-1 from: world.synthetic_population at individual_type.key;}}}}
						
		return real_family_size;}
						
	action assign_individual_inside(individual person, house a_home) {
		ask person {
			self.home <- a_home;
			self.current_objective <- a_home;
			self.location <- any_location_in(a_home);
			add self to: a_home.inhabitants;
			ask a_home {do visitor_arrived(myself);}}
	}
	
	/********************************* Working *********************************/
	concrete_target get_working_place_for(individual person) {
		list<building> working_places <- building where (each.recurring_visitors_amount() < each.maximum_staff_amount);
		concrete_target working_place;
		if working_places != [] {
			working_place <- one_of(working_places);
		} else {
			working_place <- one_of(house where (each distance_to person < 10#km));
		}
		return working_place;
	}
	
	/********************************* Spatial *********************************/
	list<individual> individuals_around_person(individual me, float distance) {
		list<individual> possible_infections <- concrete_target(me.current_objective).current_visitors();
		list<individual> filtered_individuals;
		try {ask me {filtered_individuals <- (possible_infections at_distance distance) where ((each distance_to me) > 0.0);}} catch {filtered_individuals <- [];}
		return filtered_individuals;
	}
	
	list<individual> individuals_around_vehicle(vehicle me, float distance) {
		list<individual> filtered_individuals;
		try {ask me {filtered_individuals <- (individual at_distance distance) where ((each distance_to me) > 0.0);}} catch {filtered_individuals <- [];}
		return filtered_individuals;
	}
	
	float affect_rate_by_icv(individual person, float rate) {
		float closest_icv <- person.icv_value();
		float deviation <- (max_icv - closest_icv) / 1000;
		return rate + deviation;
	}
	
	/********************************* Geometry ********************************/
	point transform_to_output_crs(point original_point) {
		return point(CRS_transform(original_point, coordinate_system_output));}
	
	string point_as_string(point original_point) {return ""+ original_point.x +" "+ original_point.y;}
	
	string list_as_delimited_string(list<unknown> a_list, string delimiter) {
		string linestring <- "";
		string current <- "";
		int bound <- length(a_list)-1;
		loop index from: 0 to: bound {
			current <- string(a_list at index);
			if index = bound {linestring <- linestring + current;}
			else {linestring <- linestring + current + delimiter;}}
		return linestring;}
	
	string as_point_list(geometry polyline) {
		list<string> transformed_points <- polyline.points collect point_as_string(transform_to_output_crs(each));
		return list_as_delimited_string(transformed_points, ", ");}
	
	string as_wkt_point(point original_point) {
		point transformed_point <- transform_to_output_crs(original_point);
		return "POINT("+ point_as_string(transformed_point) +")";}
		
	string as_wkt_linestring(geometry polyline) {
		return "LINESTRING("+ as_point_list(polyline) +")";}
		
	string as_wkt_polygon(geometry polygon) {
		return "POLYGON(("+ as_point_list(polygon) +"))";}
	
	/********************************** Aspect *********************************/
	rgb current_color_of(float value_to_compare, float max_value, list<rgb> colors) {
		rgb current_color;
		float step_value <- max_value / 10.0;
		switch value_to_compare {
    		match_between [0*step_value,1*step_value] {current_color <- colors at 0;} 
    		match_between [1*step_value,2*step_value] {current_color <- colors at 1;}
    		match_between [2*step_value,3*step_value] {current_color <- colors at 2;}
    		match_between [3*step_value,4*step_value] {current_color <- colors at 3;}
    		match_between [4*step_value,5*step_value] {current_color <- colors at 4;}
    		match_between [5*step_value,6*step_value] {current_color <- colors at 5;} 
    		match_between [6*step_value,7*step_value] {current_color <- colors at 6;}
    		match_between [7*step_value,8*step_value] {current_color <- colors at 7;}
    		match_between [8*step_value,9*step_value] {current_color <- colors at 8;}
    		match_between [9*step_value,10*step_value] {current_color <- colors at 9;}
    	}
		return current_color;
	}
	
	/********************************* XML Parser *******************************/
	species XMLTag {
		string name;
		map<string,string> xml_attributes;
		pair tag_position;
		bool has_end_tag;
		list childs;
		string value;
		
		init {xml_attributes <- []; childs <- []; value <- '';}
		
		map as_map {
			return [
				'tag'::name,
				'attributes'::xml_attributes,
				'value'::value,
				'childs'::list(childs)];}
	}
	
	species XMLFile {
		string raw_file;
		map parsed_result;
		string tag_name_to_search;
		
		init {tag_name_to_search <- '';}
	
		XMLTag opening_tag_on(string xml_contents) {
			int tag_start <- (xml_contents index_of '<');
			int tag_end <- (xml_contents index_of '>') + 1;
			string inside_tag <- copy_between(xml_contents, tag_start+1, tag_end-1);
			pair<string,map> structure <- build_tag_structure(inside_tag);
			XMLTag tag;
			create XMLTag {
				self.name <- structure.key;
				self.xml_attributes <- structure.value;
				self.tag_position <- tag_start::tag_end;
				self.has_end_tag <- last(inside_tag) != '/';
				tag <- self;}
			return tag;}
		
		pair<string,map> build_tag_structure(string inside_tag) {
			if last(inside_tag) = '/' {
				inside_tag <- copy_between(inside_tag, 0, length(inside_tag)-1);}
			list segments <- inside_tag split_with(' ', true);
			map tag_attributes;
			list<string> sanitized_segments;
			string tag_name <- first(segments);
			remove tag_name from: segments;
			
			string current_segment <- '';
			loop segment over: segments {
				if segment contains '=' {
					if current_segment = '' {
						current_segment <- segment;} 
					else {
						add current_segment to: sanitized_segments;
						current_segment <- segment;}}
				else {current_segment <- current_segment+' '+segment;}}
			if current_segment != '' {add current_segment to: sanitized_segments;}
			
			loop segment over: sanitized_segments {
				list attribute_data <- segment split_with('=', true);
				string attribute_value <- last(attribute_data);
				tag_attributes[first(attribute_data)] <- copy_between(attribute_value, 1, length(attribute_value)-1);}
			return (tag_name::tag_attributes);}
	
		pair<int, int> next_closing_tag_position(XMLTag opening_tag, string xml_contents) {
			int tag_start <- (xml_contents index_of ('</'+opening_tag.name+'>'));
			int tag_end <- tag_start + length(opening_tag.name) + 3;
			return tag_start::tag_end;}
		
		pair<map,string> next_xml_tag_in(string xml_contents) {
			XMLTag opening_tag <- opening_tag_on(xml_contents);
			string remaining <- '';
			if opening_tag.has_end_tag {
				pair closing_position <- next_closing_tag_position(opening_tag, xml_contents);
				string tag_contents <- copy_between(xml_contents, int(opening_tag.tag_position.value), closing_position.key);
				if first(tag_contents) = '<' {
					loop while: (length(tag_contents) > 0) {
						pair<map,string> next_step <- next_xml_tag_in(tag_contents);
						ask opening_tag {add next_step.key to: self.childs;}
						tag_contents <- next_step.value;}
				} else {
					ask opening_tag {self.value <- tag_contents;}
				}
				remaining <- copy_between(xml_contents, closing_position.value, length(xml_contents));}
			else {remaining <- copy_between(xml_contents, int(opening_tag.tag_position.value), length(xml_contents));}
			return opening_tag.as_map()::remaining;
		}
		
		XMLFile parse {
			string xml_contents <- copy_between(raw_file, 0, length(raw_file));
			pair<map,string> parsed_xml <- next_xml_tag_in(xml_contents);
			self.parsed_result <- parsed_xml.key;
			return self;}
		
		XMLFile tag_named(string tag_name) {self.tag_name_to_search <- tag_name; return self;}
		
		unknown convert_value(map tag) {
			string value_to_convert <- tag['attributes']['value'];
			string value_type <- tag['attributes']['type'];
			unknown result;
			switch value_type {
				match 'INT' {result <- int(value_to_convert);}
				match 'FLOAT' {result <- float(value_to_convert);}
				match 'BOOLEAN' {result <- bool(value_to_convert);}
				default {result <- value_to_convert;}}
			return result;}
		
		unknown at(list tag_path) {
			map current_step <- parsed_result;
			loop tag over: tag_path {
				int index;
				string tag_name;
				if tag is string {index <- 0; tag_name <- tag;} else {index <- int(pair(tag).value); tag_name <- pair(tag).key;}
				current_step <- (list(current_step['childs']) where (each['tag']=tag_name))[index];}
			current_step <- list(current_step['childs']) first_with (each['attributes']['name']=self.tag_name_to_search);
			self.tag_name_to_search <- '';
			return convert_value(current_step);}
			
		bool exists_at(list tag_path) {
			map current_step <- parsed_result;
			loop tag over: tag_path {
				int index;
				string tag_name;
				if tag is string {index <- 0; tag_name <- tag;} else {index <- int(pair(tag).value); tag_name <- pair(tag).key;}
				current_step <- (list(current_step['childs']) where (each['tag']=tag_name))[index];}
			return length(list(current_step['childs']) where (each['attributes']['name']=self.tag_name_to_search)) != 0;}
	}
}