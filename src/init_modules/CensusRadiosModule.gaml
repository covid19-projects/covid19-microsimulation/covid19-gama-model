/***
* Name: CensusRadiosModule
* Author: Ezequiel Puerta
* Description: Inicializador de radios censales e individuos segun capas GIS
***/

model CensusRadiosModule

import "InitializationModule.gaml"
import "../species/geography/CensusRadio.gaml"

global {
	action load_synthetic_population {
		world.individual_amount <- 0;
		loop person_data over: rows_list(matrix(csv_file(world.snapshot_file_path, ";", true))) {
			create individual {
				world.individual_amount <- world.individual_amount+1;
				int my_age <- int(person_data[1]);
				string my_sex <- string(person_data[2]);
				string my_home_name <- string(person_data[3]);
				self.contagion_type <- string(person_data[6]);
				
				create biological_organism {
					myself.bio <- self;
					self.person <- myself;
					self.configured_by_external_source <- true;
					self.state <- string(person_data[10]);
					self.will_be_asymptomatic <- bool(person_data[14]);
					self.will_die <- bool(person_data[15]);
					self.infectivity_duration <- float(person_data[16]);
					if person_data[4] != '' {self.infection_date <- date(person_data[4]);}
					if person_data[11] != '' {self.last_disease_state <- string(person_data[11]);}
					if person_data[12] != '' {self.prev_state_date <- date(person_data[12]);}
					if person_data[13] != '' {self.next_state_date <- date(person_data[13]);}}
				
				infections <- [];
				if person_data[17] != '' {
					loop person_name over: (string(person_data[17]) split_with('|', true)) {
						infections << world.individual_named(person_name);}}
				
				demographic_range my_range <- world.demographic_range_according(my_age);
				ask self {do load_demographic_data(my_range, my_age, my_sex);}
				house my_home <- world.house_named(my_home_name);
				ask world {do assign_individual_inside(myself, my_home);}}}}
	
	action generate_synthetic_population {
		if world.individual_amount <= 0 {
			world.individual_amount <- sum(census_radio collect each.population);}
		
		create individual number: world.individual_amount {
			ask self {
				do set_demographic_range(world.get_demographic_range());
				pair<string,string> synthetic_key <- self.bio.sex::self.bio.range.consider_as;
				
				if self.bio.sex = male {
					if self.bio.range.consider_as = young {
						young_male_amount <- young_male_amount +1;
					} else if self.bio.range.consider_as = adult {
						adult_male_amount <- adult_male_amount +1;
					} else {
						older_adult_male_amount <- older_adult_male_amount +1;
					}
				} else {
					if self.bio.range.consider_as = young {
						young_female_amount <- young_female_amount +1;
					} else if self.bio.range.consider_as = adult {
						adult_female_amount <- adult_female_amount +1;
					} else {
						older_adult_female_amount <- older_adult_female_amount +1;
					}
				}
				
				if (world.synthetic_population at synthetic_key) = nil {world.synthetic_population << synthetic_key::[self];} 
				else {(world.synthetic_population at synthetic_key) << self;}}}
		
		int remaining_individuals <- world.individual_amount;
		
		loop home over: shuffle(house) {
			if not(remaining_individuals > 0) {break;}
			ask home {
				int average_individuals_per_house;
				switch length(self.zone.houses) {
				   	match 0 {average_individuals_per_house <- 0;}
				   	default {average_individuals_per_house <- int(ceil(self.zone.population / length(self.zone.houses)));}}
				int family_size <- int(abs(gauss(average_individuals_per_house, 1)));
				family_size <- min(remaining_individuals, world.max_family_size, family_size>0 ? family_size : 1);
				ask world {remaining_individuals <- remaining_individuals - assign_families_inside(family_size, myself);}}}
		
		if remaining_individuals > 0 {
			pair i_key <- (''::'');
			world.synthetic_population <- [i_key::interleave(world.synthetic_population.values)];
			ask remaining_individuals among house {
				individual current_individual <- last(world.synthetic_population at i_key);
				ask world {do assign_individual_inside(current_individual, myself);}
				remove current_individual from: world.synthetic_population at i_key;}}
	}
}


species census_radios_module parent: gis_module schedules: [] {
	list<string> dependencies <- [housesModuleId];
	
	action add_houses_to(census_radio me) {
		ask house where (me intersects each) {
			add item: self to: me.houses;
			self.zone <- me;}}
	
	action initialize {
		create census_radio from: shapefile with: 
	    	[population:: int(read('TOT_POBLAC')), 
	    		houses_amount:: int(read('TOT_HOGARE')), 
	    		density:: float(read('DENSIDAD')), 
	    		icv_value:: float(read('ICV2010'))] {
			max_density <- max(max_density, self.density);
			ask myself {do add_houses_to(myself);}}
		
		if world.snapshot_file_path != '' {
			ask world {do load_synthetic_population();}
		} else {
			ask world {do generate_synthetic_population();}
			ask patient_zero_amount among individual {
				world.disease_cases_evolution << [self.name, 'Initial case', myself.name, world.as_wkt_point(self.location)];
				do get_infected_as(initial_case);
			}
		}
	}
}
