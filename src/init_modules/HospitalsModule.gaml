/***
* Name: HospitalsModule
* Author: Ezequiel Puerta
* Description: 
***/

model HospitalsModule

import "InitializationModule.gaml"
import "../species/targets/Hospital.gaml"

species hospitals_module parent: gis_module schedules: [] {
	action initialize {
		create hospital from: shapefile with: [type_activity:: hospital_b, general_capacity::int(read('NRO_CAMAS')), icu_capacity::int(read('NRO_UCI'))];
	} 
}