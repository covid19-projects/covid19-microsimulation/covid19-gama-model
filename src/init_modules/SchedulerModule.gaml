/***
* Name: SchedulerModule
* Author: Ezequiel Puerta
* Description: Inicializador del scheduler de actividades
***/

model SchedulerModule

import "InitializationModule.gaml"
import "../policies/activities/Activity.gaml"
import "../policies/restrictions/Restriction.gaml"

species daily_agenda {
	map<float, list<scheduled_task>> tasks_by_daytime <- [];
	
	list<scheduled_task> tasks_at(float daytime) {
		return self.tasks_by_daytime at daytime;}
	
	bool is_daytime_already_configured(float daytime) {
		return self.tasks_by_daytime.keys contains daytime;}
		
	action register_on_new_daytime(scheduled_task task) {
		put [task] in: self.tasks_by_daytime at: task.start_time;}
		
	action register_on_already_configured_daytime(scheduled_task task) {
		add task to: (self.tasks_by_daytime at task.start_time);}
		
	action register(scheduled_task task) {
		if self.is_daytime_already_configured(task.start_time) {
			do register_on_already_configured_daytime(task);
		} else {
			do register_on_new_daytime(task);
		}
	}
}

species weekly_agenda {
	map<int, daily_agenda> tasks_by_weekday <- [];
	
	bool has_any_tasks_on(int weekday, float daytime) {
		bool result <- false;
		if self.is_day_already_configured(weekday) {
			ask (self.tasks_by_weekday at weekday) {
				result <- self.is_daytime_already_configured(daytime);}}
		return result;}
	
	list<scheduled_task> tasks_on(int weekday, float daytime) {
		return (self.tasks_by_weekday at weekday).tasks_at(daytime);}
	
	bool is_day_already_configured(int weekday) {
		return self.tasks_by_weekday.keys contains weekday;}
	
	action register_on_new_day(scheduled_task task) {
		create daily_agenda number: 1 with: [tasks_by_daytime::map(task.start_time::[task])] {
			put self in: myself.tasks_by_weekday at: task.day;}}
	
	action register_on_already_configured_day(scheduled_task task) {
		ask (self.tasks_by_weekday at task.day) {do register(task);}}

	action register(scheduled_task task) {
		if self.is_day_already_configured(task.day) {
			do register_on_already_configured_day(task);
		} else {
			do register_on_new_day(task);
		}
	}
}

species scheduler_module parent: init_module schedules: [] {
	weekly_agenda agenda;
	list<activity> current_activities;
	list<restriction> current_restrictions;
	list<activity> non_recurrent <- [];
	list<string> dependencies <- [];
	
	init {create weekly_agenda number: 1 {myself.agenda <- self;}}
	
	action register(scheduled_task task) {
		ask agenda {do register(task);}}
	
	action initialize {
		loop current_activity over: current_activities {
			if !current_activity.is_recurrent {
				add current_activity to: non_recurrent;
			} else {
				ask recurrent_activity(current_activity) {do apply_recurrently(myself);}	
			}
		}
		
		create scheduler number: 1 with: [
			activities::current_activities, 
			restrictions::current_restrictions,
			tasks_agenda::agenda,
			non_recurrent_activities::non_recurrent]{
				world.scheduler_singleton <- self;
			}
	}
}
