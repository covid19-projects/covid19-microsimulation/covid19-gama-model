/***
* Name: TrainStationsModule
* Author: Ezequiel Puerta
* Description: Inicializador de estaciones de trenes segun capas GIS
***/

model TrainStationsModule

import "InitializationModule.gaml"
import "../species/targets/Building.gaml"

species train_stations_module parent: gis_module schedules: [] {
	action initialize {
		create building from: shapefile with: [type_activity:: station_b];
	} 
}