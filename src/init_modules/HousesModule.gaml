/***
* Name: HousesModule
* Author: Ezequiel Puerta
* Description: Inicializador de hogares segun capas GIS
***/

model HousesModule

import "InitializationModule.gaml"
import "../species/targets/House.gaml"

species houses_module parent: gis_module schedules: [] {
	action initialize {
		create house from: shapefile with: [type_activity:: house_b];
	} 
}