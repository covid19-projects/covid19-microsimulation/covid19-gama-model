/***
* Name: BlocksModule
* Author: Ezequiel Puerta
* Description: Inicializador de manzanas segun capas GIS
***/

model BlocksModule

import "InitializationModule.gaml"
import "../species/geography/Block.gaml"

species blocks_module parent: gis_module schedules: [] {
	action initialize {
		create block from: shapefile;
	} 
}