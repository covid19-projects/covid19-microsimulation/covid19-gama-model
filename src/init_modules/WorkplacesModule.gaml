/***
* Name: WorkplacesModule
* Author: Ezequiel Puerta
* Description: Inicializador de edificios laborales genéricos segun capas GIS
***/

model WorkplacesModule

import "InitializationModule.gaml"
import "../species/targets/Building.gaml"

species workplaces_module parent: gis_module schedules: [] {	
	action initialize {
		create building from: shapefile with: [
			type_activity::read('TIPO'), 
			name_id::read('NOMBRE'), 
			maximum_staff_amount::int(read('PLANTEL')),
			is_for_essential_activity::bool(read('ESENCIAL'))];
	} 
}