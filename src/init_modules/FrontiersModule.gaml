/***
* Name: FrontiersModule
* Author: Ezequiel Puerta
* Description: Inicializador de accesos fronterizos segun capas GIS
***/

model FrontiersModule

import "InitializationModule.gaml"
import "../species/targets/Frontier.gaml"

species frontiers_module parent: gis_module schedules: [] {
	list<string> dependencies <- [routesModuleId];
	
	action initialize {
		list<frontier> all_frontiers;
		
		create frontier from: shapefile with: [
			inflow_per_hour::int(read('F_IN_HORA')),
			outflow_per_hour::int(read('F_OUT_HORA')),
			disease_rate::float(read('PROB_COVID'))] {
				add item: self to: all_frontiers;
			}
		
		int frontier_index_from_end <- length(all_frontiers)-1;
		loop frontier_from_start over: all_frontiers {
			ask frontier_from_start {do set_destination_frontier(all_frontiers[frontier_index_from_end]);}
			frontier_index_from_end <- frontier_index_from_end-1;}
		
	} 
}