/***
* Name: KidsAtPark
* Author: Ezequiel Puerta
* Description: Los chicos y jovenes se juntan
***/

model KidsAtPark

import "Activity.gaml"

species kids_at_park parent: immediate_activity {
	float duration <- 3 #hours;
	
	init {
		type <- playing_at_park;
		min_start_hour <- 12 #hours;
		max_start_hour <- 19 #hours;
		granularity <- 30 #minutes;
	}
	
	bool finished(scheduled_task current_task) {
		assert(self.type = current_task.task.type);
		return world.scheduler_singleton.current_time >= current_task.finish_time;
	}
	
	list<individual> concerning_population {
		return int((young_male_amount + young_female_amount) / 200) among shuffle(individual where (each.is_resting() and each.bio.range.is_young()));
	}
	
	list<scheduled_task> generate_tasks_for(individual person) {
		list<scheduled_task> tasks <- [];
		float start_time <- world.scheduler_singleton.current_time;
		float finish_time <- start_time + self.duration;
		concrete_target objective <- one_of(building where (each.type_activity = park_b));
		tasks << create_certain_task(world.scheduler_singleton.current_week_day, start_time, finish_time, person, objective);
		return tasks;
	}
}