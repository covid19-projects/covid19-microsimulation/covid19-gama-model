/***
* Name: GoToWork
* Author: Ezequiel Puerta
* Description: Trabajos genericos
***/

model GenericWork

import "Activity.gaml"

species generic_work parent: recurrent_activity {
	list<float> probability_thresholds <- [];
	list<list<float>> durations <- [[4 #hours, 0, 0], [6 #hours, 0, 0] , [8 #hours, 1, 1]]; //work duration, occurrence probability, weekend probability
	
	init {
		type <- working;
		min_start_hour <- 4 #hours;
		max_start_hour <- 10 #hours;
		granularity <- 30 #minutes;
			
		float current_threshold <- 0.0;	
		loop each_duration over: durations {
			current_threshold <- current_threshold + (each_duration at 1);
			add current_threshold to: probability_thresholds;
		}
	}
	
	bool finished(scheduled_task current_task) {
		assert(self.type = current_task.task.type);
		return world.scheduler_singleton.current_time >= current_task.finish_time;
	}
	
	list<individual> concerning_population {
		return individual where each.is_unemployed();
	}
	
	list<scheduled_task> generate_tasks_for(individual person) {
		list<scheduled_task> tasks <- [];
		list<int> selected_days <- week;
		float result <- rnd(1.0);
		int index <- 0;
		loop threshold over: probability_thresholds {
			if result <= threshold {break;} else {index <- index+1;}}
		list<float> selected_duration <- durations at index;
		
		if flip(selected_duration at 2) {
			selected_days <- selected_days + weekend;}
		
		float start_time <- min_start_hour + (rnd(time_interval()) * granularity);
		float finish_time <- start_time + selected_duration at 0;
		concrete_target working_place <- world.get_working_place_for(person);
		loop selected_day over: selected_days {			
			tasks << create_certain_task(selected_day, start_time, finish_time, person, working_place);
		}
		return tasks;
	}
}