/***
* Name: SelfQuarantine
* Author: Ezequiel Puerta
* Description: Ante sintomas de enfermedad, el individuo se auto-recluye en su hogar.
***/

model SelfQuarantine

import "Activity.gaml"

species self_quarantine parent: immediate_activity {
	float duration <- mean_incubation_days + mean_mild_recovery_days;
	
	init {
		min_start_hour <- 0 #hours;
		max_start_hour <- 24 #hours;
		granularity <- 10 #minutes;
		type <- quarantine;
	}
	
	bool finished(scheduled_task current_task) {
		assert(self.type = current_task.task.type);
		return current_date >= (#epoch + current_task.finish_time);
	}
	
	list<individual> concerning_population {
		return individual where (each.bio.is_mild_symptomatic() and (each.is_resting() and (!each.is_in_quarantine() and flip(self_quarantine_compliance_ratio))));
	}
	
	list<scheduled_task> generate_tasks_for(individual person) {
		list<scheduled_task> tasks <- [];
		float finish_date <- float(current_date + self.duration - #epoch);
		tasks << create_certain_task(world.scheduler_singleton.current_week_day, world.scheduler_singleton.current_time, finish_date, person, person.home);
		return tasks;
	}
}