/***
* Name: KidsAtSchool
* Author: Ezequiel Puerta
* Description: Les chiques van a la escuela
***/

model KidsAtSchool

import "Activity.gaml"

species kids_at_school parent: recurrent_activity {
	list<float> probability_thresholds <- [];
	list<list<float>> durations <- [[5 #hours, 0.7, 0.1], [9 #hours, 0.3, 0.2]]; //work duration, occurrence probability, weekend probability
	
	init {
		type <- studying;
		min_start_hour <- 7 #hours;
		max_start_hour <- 8 #hours;
		granularity <- 5 #minutes;
			
		float current_threshold <- 0.0;	
		loop each_duration over: durations {
			current_threshold <- current_threshold + (each_duration at 1);
			add current_threshold to: probability_thresholds;
		}
	}
	
	bool finished(scheduled_task current_task) {
		assert(self.type = current_task.task.type);
		return world.scheduler_singleton.current_time >= current_task.finish_time;
	}
	
	list<individual> concerning_population {
		return individual where each.is_young();
	}
	
	list<scheduled_task> generate_tasks_for(individual person) {
		list<scheduled_task> tasks <- [];
		list<int> selected_days <- week;
		float result <- rnd(1.0);
		int index <- 0;
		loop threshold over: probability_thresholds {
			if result <= threshold {break;} else {index <- index+1;}}
		list<float> selected_duration <- durations at index;
		
		if flip(selected_duration at 2) {
			selected_days <- selected_days + weekend;}
		
		float start_time <- min_start_hour + (rnd(time_interval()) * granularity);
		float finish_time <- start_time + selected_duration at 0;
		concrete_target objective <- one_of(building where (each.type_activity in [kindergarden_b, school_b, high_school_b])); // esto se podria hacer mas especifico por edades...
		loop selected_day over: selected_days {			
			tasks << create_certain_task(selected_day, start_time, finish_time, person, objective);
		}
		return tasks;
	}
}