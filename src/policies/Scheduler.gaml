/***
* Name: Scheduler
* Author: Ezequiel Puerta
* Description: Planificador de actividades y validador de politicas
***/

model Scheduler

import "ScheduledTask.gaml"
import "activities/Activity.gaml"
import "restrictions/Restriction.gaml"

species scheduler {
	weekly_agenda tasks_agenda;
	list<activity> activities;
	list<restriction> restrictions;
	list<activity> non_recurrent_activities;

	float current_time <- 0.0;
	int current_week_day <- monday;

	reflex update_current_time {
		if (current_time + step >= (1 #day)) {
			current_time <- 0.0;
			current_week_day <- (current_week_day+1) mod 7;
		} else {
			current_time <- current_time + step;
		}
	}
	
	reflex schedulling {
		loop non_recurrent_activity over: non_recurrent_activities {
			ask immediate_activity(non_recurrent_activity) {do apply_immediately();}}
		
		if tasks_agenda.has_any_tasks_on(current_week_day, current_time) {
			loop task over: tasks_agenda.tasks_on(current_week_day, current_time) {
				ask task {do execute();}}}}
					
	bool is_restricted(individual person) {
		bool restricted;
		if flip(restrictions_compliance_ratio) {
			restricted <- true;
			loop each_restriction over: (restrictions where each.is_active) {
				ask each_restriction {restricted <- restricted and restricting_to(person);}} // all restrictions must by true to restric the movement of a person
		} else {
			restricted <- false;
		}
		return restricted;
	}
}