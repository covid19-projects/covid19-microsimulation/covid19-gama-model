/***
* Name: ScheduledTask
* Author: Ezequiel Puerta
* Description: Actividad agendada
***/

model ScheduledTask

import "../species/agents/Individual.gaml"

species scheduled_task control: fsm {
	int day;
	float start_time;
	float finish_time;
	individual person;
	abstract_target objective;
	activity task;
	point target;
	bool restricted;
	
	action execute {
		if self.person.is_resting() or self.task.for_treatment() {
			if self.person.is_doing_something() and (self.person.doing.task.for_quarantine() and self.task.for_treatment()) {
				ask self.person.doing {do die;}}
			do restart();
			self.person.doing <- self;}}
	
	bool is_idle {return self.state = idle;}
	bool is_pre_executing {return self.state = pre_executing;}
	bool is_executing {return self.state = executing;}
	bool is_post_executing {return self.state = post_executing;}
	bool is_moving {return self.is_pre_executing() or self.is_post_executing();}
	
	action restart {state <- idle;}
	
	/* FSM */
	state idle initial: true {
		enter {}
		
		transition to: pre_executing when: self.person.doing = self;
		transition to: finished when: self.person.bio.is_dead();
	}
	
	state pre_executing {
		enter {
			restricted <- world.scheduler_singleton.is_restricted(self.person);
			if not(restricted) {
				if self.person.current_objective intersects self.person.location {
					ask self.person.current_objective {do leaves(myself.person);}}
				self.person.current_objective <- self.objective;
				self.target <- any_location_in(self.objective.shape);}}
		
		transition to: pre_finish when: restricted;
		transition to: executing when: not(restricted) and (self.person.location = self.target);
		transition to: finished when: self.person.bio.is_dead();
	}
	
	state executing {
		enter {
			bool must_advance <- false;
			ask self.person.current_objective {do arrives(myself.person);}}
		
		must_advance <- bool(self.task.finished(self));
		
		exit {ask self.person.current_objective {do leaves(myself.person);}}
		
		transition to: post_executing when: must_advance;
		transition to: finished when: self.person.bio.is_dead();
	}
	
	state post_executing {
		enter {
			self.person.current_objective <- self.person.home;
			self.target <- any_location_in(self.person.home.shape);}
		
		transition to: pre_finish when: (self.person.home intersects self.person.location);
		transition to: finished when: self.person.bio.is_dead();
	}
	
	state pre_finish {
		enter {
			bool will_be_repeated <- not(self.person.doing.task.for_quarantine()) and not(self.person.doing.task.for_treatment());}
		
		exit {self.person.doing <- nil;}
		
		transition to: idle when: will_be_repeated;
		transition to: finished when: !will_be_repeated or self.person.bio.is_dead();
	}
	
	state finished final: true {
		enter {do die;}
	}
}