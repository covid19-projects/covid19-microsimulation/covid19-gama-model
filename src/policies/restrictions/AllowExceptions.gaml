/***
* Name: AllowExceptions
* Author: Ezequiel Puerta
* Description: Permite que exceptuados puedan trasladarse mas alla de lo que indica la restriccion de distancia radial (u otras)
***/

model AllowExceptions

import "Restriction.gaml"

species allow_exceptions parent: restriction {	
	bool restricting_to(individual person) {
		return !person.doing.objective.is_for_essential_activity;
	}
}