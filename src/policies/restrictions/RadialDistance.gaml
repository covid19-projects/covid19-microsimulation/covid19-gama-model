/***
* Name: RadialDistance
* Author: Ezequiel Puerta
* Description: Traslados permitidos dentro de un radio dado
***/

model RadialDistance

import "Restriction.gaml"

species radial_distance parent: restriction {	
	bool restricting_to(individual person) {
		return !(person.doing.task.for_quarantine() or person.doing.task.for_treatment()) and ((person distance_to person.current_objective) > travel_radius);
	}
}