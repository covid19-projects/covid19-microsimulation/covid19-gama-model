/***
* Name: NoValidations
* Author: Ezequiel Puerta
* Description: Sin restricciones
***/

model NoRestrictions

import "Restriction.gaml"

species no_restriction parent: restriction {
	bool restricting_to(individual person) {return false;}
}