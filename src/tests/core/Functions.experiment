/***
* Name: Functions
* Author: Ezequiel Puerta
* Description: Testing de las funciones generales
***/

model FunctionsTest

import "../../core/Functions.gaml"

experiment FunctionsTest type: test autorun: true {
	//*************************** XMLTag with value ***************************//
	test "XMLTag creation with value" {				
		string tag_name <- 'Simulation';
		map the_attributes <- map(['id'::'0', 'finalStep'::'50']);
		pair position <- (1::10);
		bool it_has_end_tag <- true;
		string the_value <- 'Executed';
		XMLTag tag;
		create XMLTag {
			self.name <- tag_name;
			self.xml_attributes <- the_attributes;
			self.tag_position <- position;
			self.has_end_tag <- it_has_end_tag;
			self.value <- the_value;
			tag <- self;
		}
		assert tag.name = tag_name;
		assert tag.xml_attributes = the_attributes;
		assert tag.tag_position = position;
		assert tag.has_end_tag = it_has_end_tag;
		assert tag.childs = [];
		assert tag.value = the_value;
		assert tag.as_map() = map(['tag'::tag_name, 'attributes'::the_attributes, 'childs'::[], 'value'::the_value]);
	}
	
	//*************************** XMLTag with childs ***************************//
	test "XMLTag creation with childs" {
		map child1 <- ['tag'::'Child1', 'attributes'::['id'::'1'], 'childs'::[], 'value'::'Development'];
		map child2 <- ['tag'::'Child2', 'attributes'::['id'::'2'], 'childs'::[], 'value'::'Testing'];
		map child3 <- ['tag'::'Child3', 'attributes'::['id'::'3'], 'childs'::[], 'value'::'Deployment'];
				
		string tag_name <- 'Simulation';
		map the_attributes <- map(['id'::'0', 'finalStep'::'50']);
		pair position <- (1::10);
		bool it_has_end_tag <- true;
		XMLTag tag;
		create XMLTag {
			self.name <- tag_name;
			self.xml_attributes <- the_attributes;
			self.tag_position <- position;
			self.has_end_tag <- it_has_end_tag;
			self.childs <- [child1, child2, child3];
			tag <- self;
		}
		assert tag.name = tag_name;
		assert tag.xml_attributes = the_attributes;
		assert tag.tag_position = position;
		assert tag.has_end_tag = it_has_end_tag;
		assert tag.childs = [child1, child2, child3];
		assert tag.value = '';
		assert tag.as_map() = ['tag'::tag_name, 'attributes'::the_attributes, 'childs'::[child1, child2, child3], 'value'::''];
	}
		
	//*************************** opening_tag_on ***************************//
	test "Simple dual tag - opening_tag_on" {
		string xml_file_contents <- '<Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		XMLTag tag <- xml.opening_tag_on(xml_file_contents);
		assert tag.name = 'Experiment_plan';
		assert tag.xml_attributes = map([]);
		assert tag.tag_position = (0::length(xml_file_contents));
		assert tag.has_end_tag = true;
		assert tag.childs = [];
		assert tag.value = '';
	}
	
	test "Simple single tag - opening_tag_on" {
		string xml_file_contents <- '<Experiment_plan/>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		XMLTag tag <- xml.opening_tag_on(xml_file_contents);
		assert tag.name = 'Experiment_plan';
		assert tag.xml_attributes = map([]);
		assert tag.tag_position = (0::length(xml_file_contents));
		assert tag.has_end_tag = false;
		assert tag.childs = [];
		assert tag.value = '';
	}
	
	test "Complex dual tag - opening_tag_on" {
		string xml_file_contents <- '<Simulation id="0" experiment="ParameterOptimization" finalStep="24">';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		XMLTag tag <- xml.opening_tag_on(xml_file_contents);
		assert tag.name = 'Simulation';
		assert tag.xml_attributes = ['id'::'0', 'experiment'::'ParameterOptimization', 'finalStep'::'24'];
		assert tag.tag_position = (0::length(xml_file_contents));
		assert tag.has_end_tag = true;
		assert tag.childs = [];
		assert tag.value = '';		
	}
	
	test "Complex single tag - opening_tag_on" {
		string xml_file_contents <- '<Simulation id="0" experiment="ParameterOptimization" finalStep="24"/>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		XMLTag tag <- xml.opening_tag_on(xml_file_contents);
		assert tag.name = 'Simulation';
		assert tag.xml_attributes = ['id'::'0', 'experiment'::'ParameterOptimization', 'finalStep'::'24'];
		assert tag.tag_position = (0::length(xml_file_contents));
		assert tag.has_end_tag = false;
		assert tag.childs = [];
		assert tag.value = '';	
	}
	
	//*************************** next_closing_tag_position ***************************//
	test "Simple dual tag - next_closing_tag_position" {
		string xml_file_contents <- '<Experiment_plan></Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<int,int> closing_position <- xml.next_closing_tag_position(xml.opening_tag_on(xml_file_contents), xml_file_contents);
		assert closing_position.key = 17;
		assert closing_position.value = 35;
	}
	
	test "Complex dual tag - next_closing_tag_position" {
		string xml_file_contents <- '<Simulation id="0"></Simulation>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<int,int> closing_position <- xml.next_closing_tag_position(xml.opening_tag_on(xml_file_contents), xml_file_contents);
		assert closing_position.key = 19;
		assert closing_position.value = 32;
	}
	
	test "Simple dual tag with contents - next_closing_tag_position" {
		string xml_file_contents <- '<Experiment_plan><Data id="0"/><Data id="1"/></Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<int,int> closing_position <- xml.next_closing_tag_position(xml.opening_tag_on(xml_file_contents), xml_file_contents);
		assert closing_position.key = 45;
		assert closing_position.value = 63;
	}
	
	test "Complex dual tag with contents - next_closing_tag_position" {
		string xml_file_contents <- '<Simulation id="0"><Data id="0"/><Data id="1"/></Simulation>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<int,int> closing_position <- xml.next_closing_tag_position(xml.opening_tag_on(xml_file_contents), xml_file_contents);
		assert closing_position.key = 47;
		assert closing_position.value = 60;
	}
	
	//*************************** next_xml_tag_in ***************************//
	test "Simple single tag - next_xml_tag_in" {
		string xml_file_contents <- '<Fake1/>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<map,string> tag_and_remaining <- xml.next_xml_tag_in(xml_file_contents);
		assert tag_and_remaining.key = ['tag'::'Fake1', 'attributes'::map([]), 'childs'::[], 'value'::''];
		assert tag_and_remaining.value = '';
	}
	
	test "Two single tag - next_xml_tag_in" {
		string xml_file_contents <- '<Fake1/><Fake2/>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<map,string> tag_and_remaining <- xml.next_xml_tag_in(xml_file_contents);
		assert tag_and_remaining.key = ['tag'::'Fake1', 'attributes'::map([]), 'childs'::[], 'value'::''];
		assert tag_and_remaining.value = '<Fake2/>';
	}
	
	test "Simple dual tag - next_xml_tag_in" {
		string xml_file_contents <- '<Experiment_plan></Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<map,string> tag_and_remaining <- xml.next_xml_tag_in(xml_file_contents);
		assert tag_and_remaining.key = ['tag'::'Experiment_plan', 'attributes'::map([]), 'childs'::[], 'value'::''];
		assert tag_and_remaining.value = '';
	}
	
	test "Two simple dual tags - next_xml_tag_in" {
		string xml_file_contents <- '<Experiment_plan></Experiment_plan><Experiment_plan></Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<map,string> tag_and_remaining <- xml.next_xml_tag_in(xml_file_contents);
		assert tag_and_remaining.key = ['tag'::'Experiment_plan', 'attributes'::map([]), 'childs'::[], 'value'::''];
		assert tag_and_remaining.value = '<Experiment_plan></Experiment_plan>';
	}
	
	test "Complex dual tag - next_xml_tag_in" {
		string xml_file_contents <- '<Experiment_plan id="0"><Fake1/><Fake2/></Experiment_plan>';
		map fake1 <- ['tag'::'Fake1', 'attributes'::map([]), 'childs'::[], 'value'::''];
		map fake2 <- ['tag'::'Fake2', 'attributes'::map([]), 'childs'::[], 'value'::''];
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<map,string> tag_and_remaining <- xml.next_xml_tag_in(xml_file_contents);
		assert tag_and_remaining.key = ['tag'::'Experiment_plan', 'attributes'::['id'::'0'], 'childs'::[fake1, fake2], 'value'::''];
		assert tag_and_remaining.value = '';
	}
	
	test "Two complex dual tags - next_xml_tag_in" {
		string xml_file_contents <- '<Experiment_plan id="0"><Fake1/><Fake2/></Experiment_plan><Experiment_plan id="1"><Fake3/><Fake4/></Experiment_plan>';
		map fake1 <- ['tag'::'Fake1', 'attributes'::map([]), 'childs'::[], 'value'::''];
		map fake2 <- ['tag'::'Fake2', 'attributes'::map([]), 'childs'::[], 'value'::''];
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		pair<map,string> tag_and_remaining <- xml.next_xml_tag_in(xml_file_contents);
		assert tag_and_remaining.key = ['tag'::'Experiment_plan', 'attributes'::['id'::'0'], 'childs'::[fake1, fake2], 'value'::''];
		assert tag_and_remaining.value = '<Experiment_plan id="1"><Fake3/><Fake4/></Experiment_plan>';
	}
	
	//*************************** parse ***************************//
	//	<Experiment_plan>
	//		<Mock/>
	//	    <Simulation id="0" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="ParameterOptimization" finalStep="24">
	//	        <Parameters>
	//	            <Parameter name="individual_amount" type="INT" value="5000" />
	//	            <Parameter name="starting_date" type="STRING" value="2020-04-27 00:00:00" />
	//	        </Parameters>
	//	        <Outputs>
	//				<Output id="1" name="#Susceptible" framerate="1" />
	//			</Outputs>
	//	    </Simulation>
	//	    <Simulation id="1" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="MapUI" finalStep="48">
	//	        <Parameters>
	//	            <Parameter name="individual_amount" type="INT" value="2500" />
	//	        </Parameters>
	//	        <Outputs>
	//				<Output id="1" name="#Susceptible" framerate="1" />
	//			</Outputs>
	//	    </Simulation>
	//		<AnotherMock>mock_value</AnotherMock>
	//	</Experiment_plan>
	
	test "Parse" {
		string xml_file_contents <- '<Experiment_plan><Mock/><Simulation id="0" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="ParameterOptimization" finalStep="24"><Parameters><Parameter name="individual_amount" type="INT" value="5000" /><Parameter name="starting_date" type="STRING" value="2020-04-27 00:00:00" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><Simulation id="1" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="MapUI" finalStep="48"><Parameters><Parameter name="individual_amount" type="INT" value="2500" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><AnotherMock>mock_value</AnotherMock></Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self;}
		map result <- xml.parse().parsed_result;
		assert result = [
			'tag'::'Experiment_plan', 
			'attributes'::map([]),
			'value'::'',
			'childs'::[
				['tag'::'Mock', 
					'attributes'::map([]),
					'value'::'',
					'childs'::[]],
				['tag'::'Simulation',
					'attributes'::['id'::'0', 'sourcePath'::'/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml', 'experiment'::'ParameterOptimization', 'finalStep'::'24'],
					'value'::'',
					'childs'::[
						['tag'::'Parameters',
							'attributes'::map([]),
							'value'::'',
						 	'childs'::[
								['tag'::'Parameter',
									'attributes'::['name'::'individual_amount', 'type'::'INT', 'value'::'5000'],
									'value'::'',
									'childs'::[]],
								['tag'::'Parameter',
									'attributes'::['name'::'starting_date', 'type'::'STRING', 'value'::'2020-04-27 00:00:00'],
									'value'::'',
									'childs'::[]]]],
						['tag'::'Outputs',
							'attributes'::map([]),
							'value'::'',
							'childs'::[
								['tag'::'Output',
									'attributes'::['id'::'1', 'name'::'#Susceptible', 'framerate'::'1'],
									'value'::'',
									'childs'::[]]]]]],
				['tag'::'Simulation',
					'attributes'::['id'::'1', 'sourcePath'::'/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml', 'experiment'::'MapUI', 'finalStep'::'48'],
					'value'::'',
					'childs'::[
						['tag'::'Parameters',
							'attributes'::map([]),
							'value'::'',
						 	'childs'::[
								['tag'::'Parameter',
									'attributes'::['name'::'individual_amount', 'type'::'INT', 'value'::'2500'],
									'value'::'',
									'childs'::[]]]],
						['tag'::'Outputs',
							'attributes'::map([]),
							'value'::'',
							'childs'::[
								['tag'::'Output',
									'attributes'::['id'::'1', 'name'::'#Susceptible', 'framerate'::'1'],
									'value'::'',
									'childs'::[]]]]]],
				['tag'::'AnotherMock', 
					'attributes'::map([]), 
					'value'::'mock_value', 
					'childs'::[]]]];}
					
	test "Accessing value of first tag named" {
		string xml_file_contents <- '<Experiment_plan><Mock/><Simulation id="0" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="ParameterOptimization" finalStep="24"><Parameters><Parameter name="individual_amount" type="INT" value="5000" /><Parameter name="starting_date" type="STRING" value="2020-04-27 00:00:00" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><Simulation id="1" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="MapUI" finalStep="48"><Parameters><Parameter name="individual_amount" type="INT" value="2500" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><AnotherMock>mock_value</AnotherMock></Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self.parse();}
		assert xml.tag_named('individual_amount').at(['Simulation', 'Parameters']) = 5000;
		assert xml.tag_named('starting_date').at(['Simulation', 'Parameters']) = '2020-04-27 00:00:00';}
		
	test "Accessing value of second tag named" {
		string xml_file_contents <- '<Experiment_plan><Mock/><Simulation id="0" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="ParameterOptimization" finalStep="24"><Parameters><Parameter name="individual_amount" type="INT" value="5000" /><Parameter name="starting_date" type="STRING" value="2020-04-27 00:00:00" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><Simulation id="1" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="MapUI" finalStep="48"><Parameters><Parameter name="individual_amount" type="INT" value="2500" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><AnotherMock>mock_value</AnotherMock></Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self.parse();}
		assert xml.tag_named('individual_amount').at([('Simulation'::1), 'Parameters']) = 2500;}
		
	test "Checking existence of tag value" {
		string xml_file_contents <- '<Experiment_plan><Mock/><Simulation id="0" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="ParameterOptimization" finalStep="24"><Parameters><Parameter name="individual_amount" type="INT" value="5000" /><Parameter name="starting_date" type="STRING" value="2020-04-27 00:00:00" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><Simulation id="1" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="MapUI" finalStep="48"><Parameters><Parameter name="individual_amount" type="INT" value="2500" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><AnotherMock>mock_value</AnotherMock></Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self.parse();}
		assert xml.tag_named('individual_amount').exists_at([('Simulation'::1), 'Parameters']);}
		
	test "Checking inexistence of tag value" {
		string xml_file_contents <- '<Experiment_plan><Mock/><Simulation id="0" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="ParameterOptimization" finalStep="24"><Parameters><Parameter name="individual_amount" type="INT" value="5000" /><Parameter name="starting_date" type="STRING" value="2020-04-27 00:00:00" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><Simulation id="1" sourcePath="/mnt/Datos/GIT/covid19-gama-model/src/core/Main.gaml" experiment="MapUI" finalStep="48"><Parameters><Parameter name="individual_amount" type="INT" value="2500" /></Parameters><Outputs><Output id="1" name="#Susceptible" framerate="1" /></Outputs></Simulation><AnotherMock>mock_value</AnotherMock></Experiment_plan>';
		XMLFile xml;
		create XMLFile {self.raw_file <- xml_file_contents; xml <- self.parse();}
		assert not(xml.tag_named('inexistent_tag').exists_at([('Simulation'::1), 'Parameters']));
		assert not(xml.tag_named('individual_amount').exists_at([('Simulation')]));}
}
