/***
* Name: MonitorsExperiment
* Author: Ezequiel Puerta
* Description: Experimento con enfasis en el monitoreo de variables y evolución de los casos
***/

model MonitorsExperiment

import "../experiments/AbstractExperiment.gaml"

experiment MonitorsExperiment parent: abstract_experiment type: gui autorun: false {}