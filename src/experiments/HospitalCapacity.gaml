/***
* Name: HospitalCapacity
* Author: Ezequiel Puerta
* Description: Experimento con enfasis en la capacidad hospitalaria
***/

model HospitalCapacity

import "../experiments/AbstractExperiment.gaml"

experiment HospitalCapacity parent: abstract_experiment type: gui autorun: false {	
	/* Collect data as CSV format */
	reflex collect_csv when: world.is_time_to_collect() {
		loop sim over: ExportCSV_model {
			ask sim.house {
				ask world {do collect_csv([
					'id'::myself.name,
					'inhabitants'::length(myself.inhabitants),
					'disease_percentage'::(length(myself.inhabitants)=0 ? 0 : length(myself.inhabitants where each.bio.is_infected())/length(myself.inhabitants)),
					'geometry'::world.as_wkt_polygon(myself)], sim.name, "houses_and_disease.csv");}}}}
}