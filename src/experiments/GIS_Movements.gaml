/***
* Name: GIS_Movements
* Author: Ezequiel Puerta
* Description: Experimento GIS con enfasis en los movimientos de las personas y vehiculos
***/

model GIS_Movements

import "../experiments/AbstractExperiment.gaml"

experiment GIS_Movements parent: abstract_experiment type: gui autorun: false {	
	/* Collect data as CSV format */
	reflex collect_csv when: world.is_time_to_collect() {
		loop sim over: ExportCSV_model {
			ask sim.individual {
				point transformed_point <- world.transform_to_output_crs(self.location);
				ask world {do collect_csv([
					'current_date'::string(current_date),
					'id'::myself.name,
					'age'::myself.bio.age, 
					'sex'::myself.bio.sex,
					'state'::myself.bio.state,
					'state_code'::myself.bio.state_code,
					'latitude'::transformed_point.y,
					'longitude'::transformed_point.x,
					'home'::myself.home.name,
					'objective'::myself.current_objective.name], sim.name, "individuals.csv");}}
							
			ask sim.vehicle {
				point transformed_point <- world.transform_to_output_crs(self.location);
				point transformed_origin <- world.transform_to_output_crs(self.origin);
				point transformed_target <- world.transform_to_output_crs(self.target);
				ask world {do collect_csv([
					'current_date'::string(current_date),
					'id'::myself.name, 
					'latitude'::transformed_point.y,
					'longitude'::transformed_point.x,
					'latitude-origin'::transformed_origin.y,
					'longitude-origin'::transformed_origin.x,
					'latitude-target'::transformed_target.y,
					'longitude-target'::transformed_target.x,
					'driver_exposed'::myself.driver_exposed], sim.name, "vehicles.csv");}}}}
}