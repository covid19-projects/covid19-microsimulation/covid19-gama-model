# COVID19 GAMA Model
---
## Description

Micro simulator related to the evolution of **CoVid19** in georeferenced maps, according to the movements and actions of the individuals that inhabit the area.

Developed for the *pure agent-based* simulator, **GAMA Platform** (https://github.com/gama-platform/gama/releases/tag/latest). Stable version 1.8.0 is highly recommended:

## Table of Contents

1. [Installation](/docs/installation/README.md)

## Introductory video (in Spanish)

[![Introductory video (in Spanish)](/docs/yt.png)](https://www.youtube.com/watch?v=JUCZHHvoJhg)

## Used data:

- National Geographic Institute (IGN : [Instituto Geográfico Nacional](https://www.ign.gob.ar/NuestrasActividades/InformacionGeoespacial/CapasSIG))
- Quality of Life Index (Índice de Calidad de Vida), [Conicet](https://icv.conicet.gov.ar/).